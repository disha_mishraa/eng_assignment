import { API_BASE } from '../../sharedUtils/Constants/constants';
export const get = url => {
  return fetch(API_BASE + url, {
    method: 'GET',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
  })
    .then(response => response.json())
    .then(responseData => {
      return responseData;
    });
};

export const post = (url, Body) => {
  return (
    fetch(API_BASE + url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      credentials: 'include',
      body: JSON.stringify(Body),
    })
      .then(response => response.json())
      .then(responseData => {
        return responseData;
      })
  );
};
