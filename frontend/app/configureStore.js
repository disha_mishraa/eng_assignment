import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';
import movies from './containers/HomePage/redux/HomePage';

const reducer = combineReducers({
  // here we will be adding reducers
  movies,
});
const store = configureStore({
  reducer,
});
export default store;
