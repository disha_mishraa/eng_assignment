export const API_BASE = 'http://localhost:8087/api/';
export const IMAGE_BASE = 'https://image.tmdb.org/t/p/';
export const IMAGE_SIZE = 'w440_and_h660_face';
