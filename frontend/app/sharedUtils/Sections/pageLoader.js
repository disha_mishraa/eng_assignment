import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme) => ({
  loaderBody: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    height: "100vh",
  },
  progress: {
    margin: theme.spacing(2),
  },
}));

export default function CircularIndeterminate(props) {
  const classes = useStyles();

  return (
    <div className={classes.loaderBody}>
      <CircularProgress className={classes.progress} />
      {props.text}
    </div>
  );
}
