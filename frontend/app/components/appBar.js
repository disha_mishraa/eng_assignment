import React from "react";
import IconButton from "@material-ui/core/IconButton";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import MoreVertIcon from "@material-ui/icons/MoreVert";
const AppBar = (props) => (
  <div className="appBar col-md-12">
    <IconButton
      style={{
        fontSize: "medium",
        color: "white",
      }}
    >
      {props.isNotHome && <ArrowBackIcon onClick={props.onClick} />}
      <b>{props.text}</b>
    </IconButton>
    <IconButton
      style={{
        float: "right",
        color: "white",
      }}
    >
      <MoreVertIcon />
    </IconButton>
  </div>
);
export default AppBar;
