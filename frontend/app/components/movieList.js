import React from "react";
import { IMAGE_BASE, IMAGE_SIZE } from "../sharedUtils/Constants/constants";
const MoviesList = (props) => {
  return (
    <div className="row">
      {props.movies.map((singleMovie, index) => (
        <div
          id={index}
          onClick={() => props.history.push(`movie/${singleMovie._id}`)}
          className="col"
        >
          <img
            src={IMAGE_BASE + IMAGE_SIZE + singleMovie.backdrop_path}
            alt="movie"
          />
        </div>
      ))}
    </div>
  );
};

export default MoviesList;
