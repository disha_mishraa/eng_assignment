import React, { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getMoviesList } from "./redux/HomePage";

import MovieList from "../../components/movieList";
import AppBar from "../../components/appBar";

import "bootstrap/dist/css/bootstrap.min.css";
import "../../app.css";

export default function HomePage(props) {
  const dispatch = useDispatch();
  const innerRef = useRef(null);
  const moviesList = useSelector((state) => state.movies.moviesList);

  useEffect(() => {
    dispatch(getMoviesList());
  }, []);

  return (
    <div className="container-fluid" ref={innerRef}>
      <div className="row align-items-center">
        <AppBar text="Pop Movies" />
      </div>
      {moviesList && <MovieList {...props} movies={moviesList} />}
    </div>
  );
}
