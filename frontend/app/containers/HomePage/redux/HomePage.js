import { createSlice } from "@reduxjs/toolkit";
import { get } from "../../../utils/services/api";

const slice = createSlice({
  name: "movies",
  initialState: {
    moviesList: [],
    moviesDetail: {},
    enableLoader: false,
  },
  reducers: {
    moviesList: (state, action) => {
      state.moviesList = action.payload;
    },
    movieDetail: (state, action) => {
      state.moviesDetail = action.payload.data;
    },
    enableLoader: (state, action) => {
      state.enableLoader = true;
    },
    disableLoader: (state, action) => {
      state.enableLoader = false;
    },
  },
});
export default slice.reducer;

const { moviesList, movieDetail, enableLoader, disableLoader } = slice.actions;

export const getMoviesList = (props) => async (dispatch) => {
  try {
    dispatch(enableLoader(""));
    const url = "movies/list";
    const res = await get(url);
    dispatch(disableLoader(""));
    return dispatch(moviesList(res.data));
  } catch (e) {
    return console.error(e.message);
  }
};
export const getMovieDetails = (props) => async (dispatch) => {
  try {
    dispatch(enableLoader(""));
    const url = "movies/details/" + props;
    const res = await get(url);
    dispatch(disableLoader(""));
    return dispatch(movieDetail(res));
  } catch (e) {
    return console.error(e.message);
  }
};
