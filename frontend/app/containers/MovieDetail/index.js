import React, { useEffect } from "react";
import Button from "@material-ui/core/Button";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import PlayCircleOutlineIcon from "@material-ui/icons/PlayCircleOutline";
import { useDispatch, useSelector } from "react-redux";
import CircularProgress from "@material-ui/core/CircularProgress";
import { getMovieDetails } from "../HomePage/redux/HomePage";
import AppBar from "../../components/appBar";
import { IMAGE_BASE, IMAGE_SIZE } from "../../sharedUtils/Constants/constants";

import "bootstrap/dist/css/bootstrap.min.css";
import "../../app.css";
export default function DetailesPage(props) {
  const dispatch = useDispatch();
  const movieId = props.match.params.id;
  const movieDetial = useSelector((state) => state.movies.moviesDetail);
  const isLoader = useSelector((state) => state.movies.enableLoader);
  useEffect(() => {
    dispatch(getMovieDetails(movieId));
  }, []);
  return (
    <div className="container">
      <div className="row">
        <AppBar
          isNotHome={true}
          onClick={() => props.history.push("/")}
          text="Movies Details"
        />
      </div>
      {isLoader ? (
        <div className="loader">
          {" "}
          <CircularProgress style={{ marginLeft: "45%" }} />{" "}
        </div>
      ) : (
        <>
          <div className="row">
            <div className="title col-md-12">{movieDetial.original_title}</div>
          </div>
          <div className="movieData">
            <div className="row">
              <div className="details">
                <div className="col-md-3">
                  <img
                    src={IMAGE_BASE + IMAGE_SIZE + movieDetial.poster_path}
                    alt="movie"
                  />
                </div>
                <div className="col-md-6">
                  <div className="year">{movieDetial.release_date}</div>
                  <div className="duration">
                    <em> {movieDetial.runtime} mins</em>
                  </div>
                  <div className="detail-down">
                    <div className="rating">
                      <b>{movieDetial.vote_average}/10</b>
                    </div>
                    <Button
                      style={{
                        backgroundColor: "#746A64",
                        textTransform: "unset",
                        color: "white",
                        minWidth: "110%",
                        fontSize: '16px',
                        paddingTop: "6%",
                        paddingBottom: "6%",
                      }}
                      variant="contained"
                    >
                      Add to Favorites
                    </Button>
                  </div>
                </div>
              </div>
            </div>
            <div className="description">{movieDetial.overview}</div>
            {movieDetial && movieDetial.videos && movieDetial.videos ? (
              <>
                <div className="trailer">TRAILERS</div>
                <Divider />

                <List component="nav" aria-label="main mailbox folders">
                  {movieDetial.videos.map((singleVideo) => (
                    <ListItem button className="video-list">
                      <ListItemIcon>
                        <PlayCircleOutlineIcon />
                      </ListItemIcon>
                      <ListItemText primary={singleVideo.name} />
                    </ListItem>
                  ))}
                </List>
              </>
            ) : null}
          </div>
        </>
      )}
    </div>
  );
}
