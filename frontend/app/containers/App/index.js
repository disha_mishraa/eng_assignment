import React, { Suspense } from "react";
import { Switch, Route, BrowserRouter } from "react-router-dom";
import loadable from "utils/loadable";
import PageLoader from "../../sharedUtils/Sections/pageLoader";

const HomePage = loadable(() => import("../HomePage/index"));
const MovieDetail = loadable(() => import("../MovieDetail/index"));
const PageNotFound = loadable(() => import("../../sharedUtils/Sections/pageLoader"));
export default function App(props) {
  return (
    <div>
      <Suspense fallback={<PageLoader> </PageLoader>}>
        <BrowserRouter>
          <Switch>
            <Route
              exact
              path="/"
              render={(route) => <HomePage {...route} {...props} />}
            />
            <Route
              exact
              path="/movie/:id"
              render={(route) => <MovieDetail {...route} {...props} />}
            />
            <Route component={PageNotFound} />
          </Switch>
        </BrowserRouter>
      </Suspense>
    </div>
  );
}
