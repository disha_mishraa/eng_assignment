const movieServices = require("../services");
async function getMoviesList(req, res) {
  const data = await movieServices.getMoviesList();
  res.send({
    success: true,
    data
  });
}

async function getMovieDetails(req, res) {
  const data = await movieServices.getMovieDetails(req.params.id);
  res.send({
    success: true,
    data,
  });
}

module.exports = {
  getMoviesList,
  getMovieDetails,
};
