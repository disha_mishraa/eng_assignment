const movieRoutes = require('./moviesRouting');

const combineRoutes = (app) => {
    app.use('/api/movies', movieRoutes);
};

module.exports = combineRoutes;
