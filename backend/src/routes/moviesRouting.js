const express = require('express');
const moviesController = require('../controller/movies.controllers');

const router = express.Router();

router.get('/list', moviesController.getMoviesList);
router.get('/details/:id', moviesController.getMovieDetails);

module.exports = router;
