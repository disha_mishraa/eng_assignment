const mongoose = require("mongoose");
var ObjectId = mongoose.Schema.Types.ObjectId;

const videoSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  movieId: {
    type: ObjectId,
    ref: "Movie",
  },
  iso_639_1: String,
  iso_3166_1: String,
  key: String,
  name: String,
  site: String,
  size: Number,
  type: String,
});

module.exports = mongoose.model("Video", videoSchema);
