const mongoose = require("mongoose");

const movieSchema = new mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  name: String,
  adult: Boolean,
  backdrop_path: String,
  genre_ids: [Number],
  id: Number,
  original_language: String,
  original_title: String,
  overview: String,
  release_date: Date,
  poster_path: String,
  popularity: Number,
  title: String,
  video: Boolean,
  vote_average: Number,
  vote_count: Number,
});

module.exports = mongoose.model("Movie", movieSchema);
