const {getMoviesList} = require("./moviesList");
const {getMovieDetails} = require("./moviewDetails");

module.exports = {
    getMoviesList,
    getMovieDetails,
};
