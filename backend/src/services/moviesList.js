const movieSchema = require("../models/movie")
 async function getMoviesList() {
 const data = await movieSchema.find();
  return data
}

module.exports = {
getMoviesList
}
