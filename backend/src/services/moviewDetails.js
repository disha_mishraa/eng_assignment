const mongoObject = require("mongoose").Types.ObjectId;
const movieSchema = require("../models/movie");
const videoSchema = require("../models/video");
async function getMovieDetails(id) {
  const data = await movieSchema.findById(id).lean();
  const videos = await videoSchema.find({
    movieId: mongoObject(id)
  });
  return {...data, videos};
}

module.exports = {
  getMovieDetails,
};
