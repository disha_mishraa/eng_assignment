// we will use supertest to test HTTP requests/responses
const request = require("supertest");
// we also need our app for the correct routes!
const app = require("../src/app");

describe("GET /api/movies/list ", () => {
  test("It should respond with an array of all movies", async () => {
    const response = await request(app).get("/api/movies/list");
    expect(response.body.data[0]).toHaveProperty("id");
    expect(response.body.data[0]).toHaveProperty("original_title");
  });
});

describe("GET /api/movies/details ", () => {
  test("It should respond with all details of movie - Black Widow", async () => {
    const response = await request(app).get("/api/movies/details/610162e91540787cb2b9097f");
    expect(response.body.data.original_title).toEqual("Black Widow");
  });
});
