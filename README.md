# Project Title

Engineering Assignment

---
## Requirements

For development, you will only need Node.js and a node global package.

---

## Running the project

## Backend
  
   Move to backend directory
  
   ``` cd backend ```
	
   Now install required packeges using npm
   
   ``` npm install ```
   
   And now start the backend using the following command
   
   ``` npm start ```
   
   Now the backend is running on the port 8087 in localhost
   
## Frontend
   
   Move to frontend directory from backend
  
   ``` cd frontend ```
	
   Now install required packeges using npm
   
   ``` npm install ```
   
   And now start the frontend using the following command
   
   ``` npm start ```
  
   Now the frontend is running on the port 3000 in localhost
   
## Yeah!! that's it, the project is now running on your local, have a look on [http://localhost:3000](http://localhost:3000/)